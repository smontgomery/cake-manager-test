# Waracle CakeManager Tech Test #

### Requirements ###
* [Waracle Tech Test](https://github.com/Waracle/cake-manager)

### How to Build ###
```
./gradlew clean build
```

### How to Run ###

#### Run App ####
To run app on the default port of 8080
```
./gradlew clean bootRun
```

#### Run/Configure Keycloak ####
Keycloak is required to run/configured for OAuth support:

```
docker run -d -p 8180:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/src/test/resources/cake-realm.json -e JAVA_OPTS_APPEND="-Dkeycloak.profile.feature.upload_scripts=enabled" -v $(pwd)/src/test/resources/cake-realm.json:/tmp/src/test/resources/cake-realm.json jboss/keycloak
```

Verify that Keycloak is running at http://localhost:8180/auth/admin/master/console (login admin/admin) with a new cake-realm configured with 2 users ie a cake-admin (ADMIN role) and cake-user (USER role).

#### Postman ####
A sample Postman collection can be found [here](src/test/resources/CakeManagerTest.postman_collection.json) and imported directly into Postman. NB the GET operations require user access whereas creating a cake ie (POST) requires Admin permissions - see the postman script for more details.


### Docker Images ###
To build a Docker image (requires docker to be installed locally): 
```
./gradlew bootBuildImage
```
To run built docker image:
```
docker run -p 8080:8080 --network="host" waracle/cake-mgr
```
NB the `--network="host"` arg to ensure localhost gets resolved to the host localhost - needed for the Keycloak config.

### Technologies/Frameworks Used ###
* Spring Boot
* Spring Web/MVC for Rest API
* Spring Data JPA for DB access
* Spring Validation for request validation
* Spring Test, AssertJ for testing
* Lombok
* Log4j2
* H2 Database.
* Keycloak OAuth2 Server
* Docker

### Assumptions/Considerations/Issues Encountered ###
* Initially thought about making Cake title an unique constraint but found duplicates in the sample Cake reference data so left it alone.
* Added a process to preload the Cake reference data when operating in dev mode/profile.
* Added Bitbucket CI pipeline that incorporates builds, security and SonarCloud integration. See [here for pipeline status](https://bitbucket.org/smontgomery/cake-manager-test/addon/pipelines/home) and [here for SonarCloud results](https://sonarcloud.io/dashboard?id=smontgomery_cake-manager-test)
* Original CakeMgr Jetty app wouldn't work in IntelliJ due to missing v3 web-app specification in web.xml. Didn't like Jetty 10 or 11 due to missing start.ini.


