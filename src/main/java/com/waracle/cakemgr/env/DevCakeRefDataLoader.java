package com.waracle.cakemgr.env;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.db.CakeEntity;
import com.waracle.cakemgr.db.CakeRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@Profile("dev")
@Log4j2
public class DevCakeRefDataLoader {

    private final CakeRepository cakeRepository;
    private final ObjectMapper objectMapper;
    private final String cakeRefDataUrl;

    public DevCakeRefDataLoader(CakeRepository cakeRepository, ObjectMapper objectMapper, @Value("${cake.ref.data}") String cakeRefDataUrl) {
        this.cakeRepository = cakeRepository;
        this.objectMapper = objectMapper;
        this.cakeRefDataUrl = cakeRefDataUrl;
    }

    @PostConstruct
    public List<CakeEntity> loadCannedCakes() throws IOException {

        if (0 == cakeRepository.count()) {

            log.warn("Loading dev cake data from: [{}]", cakeRefDataUrl);

            final URL cakeJson = ResourceUtils.getURL(cakeRefDataUrl);
            final List<CakeEntity> refData = objectMapper.readValue(cakeJson, new TypeReference<List<CakeEntity>>() {});
            return StreamSupport.stream(cakeRepository.saveAll(refData).spliterator(), false).collect(Collectors.toList());
        }

        return cakeRepository.findAll();
    }
}
