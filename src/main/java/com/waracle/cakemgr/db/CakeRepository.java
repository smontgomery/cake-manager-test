package com.waracle.cakemgr.db;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CakeRepository extends CrudRepository<CakeEntity, Long> {

    List<CakeEntity> findAll();
}
