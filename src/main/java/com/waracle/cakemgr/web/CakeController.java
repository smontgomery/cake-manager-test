package com.waracle.cakemgr.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.db.CakeEntity;
import com.waracle.cakemgr.db.CakeRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

@Controller
@Log4j2
public class CakeController {

    private final CakeRepository cakeRepository;
    private final ObjectMapper objectMapper;

    public CakeController(CakeRepository cakeRepository, ObjectMapper objectMapper) {
        this.cakeRepository = cakeRepository;
        this.objectMapper = objectMapper;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CakeDto>> listCakes() {

        return ResponseEntity.ok()
                .contentType(APPLICATION_JSON)
                .body(convertEntitiesToDto(cakeRepository.findAll()));
    }

    @GetMapping(value = "/cakes", produces = APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> downloadCakeFile() throws IOException {

        final List<CakeDto> cakeEntities = convertEntitiesToDto(cakeRepository.findAll());
        final byte[] jsonArray = objectMapper.writeValueAsBytes(cakeEntities);
        InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(jsonArray));

        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, "attachment; filename=cakes.json")
                .body(resource);
    }

    @RolesAllowed("ADMIN")
    @PostMapping(value = "/cakes", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CakeDto> createCake(@Valid @RequestBody CakeDto cakeDto) {

        log.debug("Creating new cake: [{}]", cakeDto);

        return ResponseEntity.ok()
                .body(CakeDto.valueOf(cakeRepository.save(cakeDto.toEntity())));
    }

    private List<CakeDto> convertEntitiesToDto(List<CakeEntity> cakeEntities) {

        return cakeEntities.stream()
                .map(CakeDto::valueOf)
                .collect(Collectors.toList());
    }
}
