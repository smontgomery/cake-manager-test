package com.waracle.cakemgr.web;

import com.waracle.cakemgr.db.CakeEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@ToString
@Builder
public class CakeDto {

    private Long id;

    @NotBlank(message = "Title is mandatory")
    private String title;

    @NotBlank(message = "Description is mandatory")
    private String description;

    @NotBlank(message = "Image is mandatory")
    private String image;

    public static CakeDto valueOf(CakeEntity cakeEntity) {

        return CakeDto.builder()
                .id(cakeEntity.getId())
                .title(cakeEntity.getTitle())
                .description(cakeEntity.getDesc())
                .image(cakeEntity.getImage())
                .build();
    }

    public CakeEntity toEntity() {

        return CakeEntity.builder()
                .id(getId())
                .title(getTitle())
                .desc(getDescription())
                .image(getImage())
                .build();
    }

}
