package com.waracle.cakemgr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.db.CakeEntity;
import com.waracle.cakemgr.db.CakeRepository;
import com.waracle.cakemgr.env.DevCakeRefDataLoader;
import com.waracle.cakemgr.web.CakeDto;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CakeControllerTests {

    private final static String REF_DATA_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CakeRepository cakeRepository;

    @BeforeEach
    private void preloadData() throws IOException {

        final List<CakeEntity> cakeEntities = new DevCakeRefDataLoader(cakeRepository, objectMapper, REF_DATA_URL).loadCannedCakes();

        assertThat(cakeEntities).hasSize(20);
    }

    @AfterEach
    private void afterTest() {
        cakeRepository.deleteAll();
    }

    @Test
    void shouldBeAuthenticatedForGetCakes() throws Exception {

        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void shouldGetCakesSuccessfully() throws Exception {

        final List<CakeDto> cakeEntityList = executeListCakes();

        verifyDefaultListResults(cakeEntityList);
    }

    private List<CakeDto> executeListCakes() throws Exception {

        final MvcResult mvcResult =  mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        return getResponseCakeList(mvcResult);
    }

    private List<CakeDto> getResponseCakeList(MvcResult mvcResult) throws JsonProcessingException, UnsupportedEncodingException {

        return objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<CakeDto>>() {});
    }

    @Test
    void shouldBeAuthenticatedForDownloadCakes() throws Exception {

        mockMvc.perform(get("/cakes"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void shouldDownloadCakesSuccessfully() throws Exception {

        final MvcResult mvcResult = mockMvc.perform(get("/cakes"))
                .andExpect(status().isOk())
                .andReturn();

        verifyDefaultListResults(getResponseCakeList(mvcResult));

        assertThat(mvcResult.getResponse().getHeader(HttpHeaders.CONTENT_DISPOSITION)).isEqualTo("attachment; filename=cakes.json");
    }

    private void verifyDefaultListResults(List<CakeDto> responseCakeList) {

        assertThat(responseCakeList)
                .hasSize(20)
                .extracting("title", "description", "image")
                .contains(new Tuple("Lemon cheesecake", "A cheesecake made of lemon", "https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg"),
                        new Tuple("Carrot cake", "Bugs bunnys favourite", "http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg"),
                        new Tuple("Banana cake", "Donkey kongs favourite", "http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg"),
                        new Tuple("victoria sponge", "sponge with jam", "http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg")
                );
    }

    @Test
    @WithMockUser
    void shouldReturnUnsupportedMediaTypeIfJsonContentTypeNotSpecifiedOnCreate() throws Exception {

        mockMvc.perform(post("/cakes"))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    @WithMockUser
    void shouldReturnBadRequestIfNoBodySpecifiedOnCreate() throws Exception {

        mockMvc.perform(post("/cakes")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void shouldReturnBadRequestIfMissingTitleOnCreate() throws Exception {

        mockMvc.perform(post("/cakes")
                .content(objectMapper.writeValueAsString(CakeDto.builder().description("testdesc").image("http://testimage").build()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void shouldReturnBadRequestIfMissingDescriptionOnCreate() throws Exception {

        mockMvc.perform(post("/cakes")
                .content(objectMapper.writeValueAsString(CakeDto.builder().title("testtitle").image("http://testimage").build()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void shouldReturnBadRequestIfMissingImageOnCreate() throws Exception {

        mockMvc.perform(post("/cakes")
                .content(objectMapper.writeValueAsString(CakeDto.builder().title("testtitle").description("testdesc").build()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldBeAuthenticatedForCreateNewCake() throws Exception {

        mockMvc.perform(post("/cakes")
                .content(objectMapper.writeValueAsString(CakeDto.builder().title("testtitle").description("testdesc").image("http://testimage").build()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void shouldBeAdminForCreateNewCake() throws Exception {

        mockMvc.perform(post("/cakes")
                .content(objectMapper.writeValueAsString(CakeDto.builder().title("testtitle").description("testdesc").image("http://testimage").build()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void shouldCreateNewCakeSuccessfully() throws Exception {

        final MvcResult mvcResult = mockMvc.perform(post("/cakes")
                .content(objectMapper.writeValueAsString(CakeDto.builder().title("testtitle").description("testdesc").image("http://testimage").build()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        final CakeDto savedEntity = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), CakeDto.class);

        assertThat(savedEntity.getId()).isGreaterThan(20);
        assertThat(savedEntity)
                .extracting("title", "description", "image")
                .containsExactly("testtitle", "testdesc", "http://testimage");

        final List<CakeDto> savedCakeList = executeListCakes();
        assertThat(savedCakeList)
                .hasSize(21)
                .extracting("title", "description", "image")
                .contains(new Tuple("testtitle", "testdesc", "http://testimage"));
    }
}
